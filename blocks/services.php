<div class="services">
    <div class="container">
        <div class="text-center">
            <br>
            <br>
            <h4>خدمات آنی مشاور</h4>
            <br>
            <p>
                آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
            </p>
        </div>
        <div class="row list">
            <div class="tab">
                <button class="tablinks" onmouseover="openCity(event, 'tab1')"><img src="images/clock.png" alt=""> مشاوره فوری</button>
                <button class="tablinks active" onmouseover="openCity(event, 'tab2')"><img src="images/calendar-with-a-clock-time-tools.png" alt=""> رزرو نوبت مشاوره</button>
                <button class="tablinks" onmouseover="openCity(event, 'tab3')"><img src="images/smartphone.png" alt=""> دانلود اپلیکیشن</button>
                <button class="tablinks" onmouseover="openCity(event, 'tab4')"><img src="images/location.png" alt=""> مشاوران نزدیک</button>
                <button class="tablinks" onmouseover="openCity(event, 'tab5')"><img src="images/support.png" alt=""> مشاوران آنلاین</button>
            </div>

            <div id="tab1" class="tabcontent">
                <h4>در کمترین زمان ممکن بهترین مشاوره را از مشاوران ما بگیرید</h4>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <img src="images/hs-1.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <div class="col-md-2">
                        <img src="images/hs-2.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <div class="col-md-2">
                        <img src="images/hs-3.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <a href="" class="btn-ani">مشاوره فوری</a>
                </div>
            </div>

            <div id="tab2" class="tabcontent active">
                <h4>در کمترین زمان ممکن بهترین مشاوره را از مشاوران ما بگیرید</h4>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <img src="images/hs-1.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <div class="col-md-2">
                        <img src="images/hs-2.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <div class="col-md-2">
                        <img src="images/hs-3.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <a href="" class="btn-ani">رزرو نوبت مشاوره</a>
                </div>
            </div>

            <div id="tab3" class="tabcontent">
                <div class="row" style="margin-right: 4.5%;">
                    <div class="col-md-8">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="text-center">
                                        <h4>اپلیکیشن آنی مشاور</h4>
                                        <br>
                                        <p>متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور
                                        متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور 
                                        </p>
                                        <br>
                                        <a href="" class="btn-ani">دانلود اپلیکیشن آنی مشاور</a>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="text-center">
                                        <h4>اپلیکیشن آنی مشاور</h4>
                                        <br>
                                        <p>متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور
                                        متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور 
                                        </p>
                                        <br>
                                        <a href="" class="btn-ani">دانلود اپلیکیشن آنی مشاور</a>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="text-center">
                                        <h4>اپلیکیشن آنی مشاور</h4>
                                        <br>
                                        <p>متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور
                                        متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور متن تستی اپلیکیشن آنی مشاور 
                                        </p>
                                        <br>
                                        <a href="" class="btn-ani">دانلود اپلیکیشن آنی مشاور</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="images/slider/slider1.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="images/slider/slider2.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="images/slider/slider3.jpg" alt="First slide">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tab4" class="tabcontent">
                <div class="row">
                    <img src="images/services/map.png" alt="">
                    <a href="" class="btn-ani">مشاهده نزدیکترین مشاوران</a>
                </div>
            </div>

            <div id="tab5" class="tabcontent">
                <h4>در کمترین زمان ممکن بهترین مشاوره را از مشاوران ما بگیرید</h4>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <img src="images/hs-1.png" alt="">
                    </div>
                    <div class="col-md-10">
                        <p>
                        آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند. 
                        </p>
                    </div>
                    <a href="" class="btn-ani">مشاوران آنلاین</a>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>