<div class="slid">
    <div class="row">
        <div class="col-md-7">

        </div>
        <div class="col-md-1"></div>
        <div class="col-md-3">
            <section id="home" class="p-0">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <!-- <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol> -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="images/slider/slider1.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/slider/slider2.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/slider/slider3.jpg" alt="Third slide">
                        </div>
                    </div>
                    <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> -->
                </div>
            </section>
        </div>
        <div class="col-md-1"></div>
    </div>

</div>
<div class="slider">
    <div class="row">
        <img src="images/slider/tem-mashverat-slider.png" alt="">
    </div>
    <div class="text-img">
        <h1>حس خوب دلگرمی به کسب و کار</h1>
        <br>
        <p>آنی مشاور فرصتی را ایجاد کرده است که اشخاص نیازمند به مشاوره به راحتی مشاورین خود را از میان چندین مشاور متخصص انتخاب کرده
            و بدون اتلاف زمان و هزینه به صورت آنلاین مشاوره بگیرند.
        </p>
        <br>
        <div class="button">
            <a href="" class="btn-ani">دریافت مشاوره رایگان</a>
        </div>
        <br>
        <div class="app">
            <ul>
                <li><a href="#"><img src="images/tem-mashverat-app-store.png" alt=""></a></li>
                <li><a href="#"><img src="images/tem-mashverat-google-play.png" alt=""></a></li>
            </ul>
        </div>
    </div>
</div>
