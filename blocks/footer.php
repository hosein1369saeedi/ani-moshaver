<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h5>تماس با آنی مشاور</h5>
                <ul>
                    <li>
                        <a href="#">
                            <img src="images/footer/icons8-phone-100.png" alt="">
                             02188321088
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/footer/icons8-phonelink-ring-100.png" alt="">
                             09128367747
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/footer/icons8-marker-100.png" alt="">
                             تهران, خیابان ۷ تیر, اول خیابان بهار شیراز, نرسیده یه خیابان سلیمان خاطر, جنب اداره پست, پلاک ۴, طبقه ۴ واحد ۱۱
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/footer/icons8-speech-bubble-100.png" alt=""> 
                             info@animoshaaver.ir
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h5>نمادها</h5>
                <div class="hs-g">
                    <div class="row">
                        <div class="col-md-6">
                            <a href=""><img src="images/footer/namad-2.jpg" alt=""></a>
                        </div>
                        <div class="col-md-6">
                            <a href=""><img src="images/footer/logo2.aspx.png" alt="" style="background:#fff"></a>
                        </div>
                        <div class="col-md-6">
                            <a href=""><img src="images/footer/logo1.png" alt="" style="background:#fff"></a>
                        </div>
                        <div class="col-md-6">
                            <a href=""><img src="images/footer/logo1.png" alt="" style="background:#fff"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h5>آنی مشاور در رسانه</h5>
                <ul class="md">
                    <li>خبرگزاری فارس جمهوری اسلامی ایران</li>
                    <li>انجمن صنفی حقوقی و شرکتها در کشور</li>
                    <li>خبرگزاری فارس جمهوری اسلامی ایران</li>
                    <li>انجمن صنفی حقوقی و شرکتها در کشور</li>
                    <li>خبرگزاری فارس جمهوری اسلامی ایران</li>
                    <li>انجمن صنفی حقوقی و شرکتها در کشور</li>
                </ul>
            </div>
        </div>
        <br>
        <div class="container-grid">
            <ul>
                <li>
                    <a href="#" class="border">
                        <img src="images/footer/icons8-facebook-f-100.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#" class="border">
                        <img src="images/footer/icons8-instagram-100.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#" class="border">
                        <img src="images/footer/icons8-telegram-app-100.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#" class="border">
                        <img src="images/footer/icons8-twitter-100.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <button onclick="topFunction()" id="myBtn" title="Go to top"></button>
</div>