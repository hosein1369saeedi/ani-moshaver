<div class="advice">
    <section id="tabs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                مراحل گرفتن مشاوره در آنی مشاور (کاربران)
                            </a>
                            <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">
                                مراحل ارایه مشاوره در آنی مشاور (مشاوران)
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <img src="images/tem-mashverat-2.png" alt="">
                            <div class="row">
                                <div class="col-md-2 text-center right">
                                    <span>ثبت نام</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>انتخاب نوع مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>انتخاب تخصص مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>انتخاب مدت مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>پرداخت هزینه مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center left">
                                    <span>انجام مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                            <img src="images/tem-mashverat-2.png" alt="">
                            <div class="row">
                                <div class="col-md-2 text-center right">
                                    <span>ثبت نام</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>انتخاب نوع مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>انتخاب تخصص مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>انتخاب مدت مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center">
                                    <span>پرداخت هزینه مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                                <div class="col-md-2 text-center left">
                                    <span>انجام مشاوره</span>
                                    <p>ابتدا با اطلاعات کاربری میتوانید وارد سایت آنی مشاور شوید و ثبت نام نمایید</p>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </section>
    <!-- ./Tabs -->
</div>