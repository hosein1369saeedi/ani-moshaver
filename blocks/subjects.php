<div class="subjects">
	<div class="text-center">
		<h4>حوزه هایی که آنی مشاور , مشاوره میدهد</h4>
	</div>
	<br>
	<br>
	<div class="container">
		<div class="row">
			<div class="slide">
				<div id="ProjeCarousel" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-conclusion-contract-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>سایر مشاوره ها</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-bill-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره مالیاتی</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-law-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره حقوقی شرکتها</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-scales-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره چک و اسناد تجاری</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-conclusion-contract-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره قراردادها</h6>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-conclusion-contract-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>سایر مشاوره ها</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-bill-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره مالیاتی</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-law-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره حقوقی شرکتها</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-scales-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره چک و اسناد تجاری</h6>
									</div>
								</div>
							</div>
							<div class="col-md-2 float-left">
								<div class="card">
									<img class="card-img-top" src="images/subjects/icons8-conclusion-contract-100.png" alt="Card image cap">
									<div class="card-body text-center">
										<h6>مشاوره قراردادها</h6>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#ProjeCarousel" role="button" data-slide="prev" style="margin-right:95.5%;margin-top: -20%;">
				<!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
				<img src="images/icon/back.png" alt="">
				<span class="sr-only">Geri</span>
			</a>
			<a class="carousel-control-next" href="#ProjeCarousel" role="button" data-slide="next" style="margin-right:-120%;margin-top: -20%;">
				<!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
				<img src="images/icon/next.png" alt="">
				<span class="sr-only">İleri</span>
			</a>
		</div>
	</div>
</div>
