<div class="menu-area fixed-top">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-light navbar-expand-lg mainmenu">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars" aria-hidden="true"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="active">
                            <a href="persondetails.php">خانه <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                آنی مشاور
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a href="#">قوانین آنی مشاور</a></li>
                                <li><a href="#">اخبار آنی مشاور</a></li>
                                <li><a href="#">درباره آنی مشاور</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        تماس با آنی مشاور
                                        <i class="fa fa-angle-down only-mobile" aria-hidden="true"></i>
                                        <i class="fa fa-angle-right only-desktop" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="blog.php">وبلاگ</a></li>
                        <li><a href="#">پرسش و پاسخ</a></li>
                    </ul>
                </div>
                <div class="login">
                    <ul>
                        <li>
                            <!-- <a class="blue" href=""><img src="images/icon/enter.png" alt=""> ورود</a> -->
                            <button type="button" class="btn blue" data-toggle="modal" data-target="#myModal2">
                                <img src="images/icon/enter.png" alt=""> ورود
                            </button>
                        </li>
                        <li>
                            <!-- <a class="green" href=""><img src="images/icon/icons8-customer-50.png" alt=""> ثبت نام</a> -->
                            <button type="button" class="btn green" data-toggle="modal" data-target="#myModal">
                                <img src="images/icon/icons8-customer-50.png" alt=""> ثبت نام 
                            </button>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- The Modal Register -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">فرم ثبت نام</h4>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <form action="/action_page.php">
                        <div class="container" style="max-width:95%;width:95%">
                            <!-- <label for="email"><b>Email</b></label> -->
                            <input type="text" placeholder="نام" name="name" required>

                            <!-- <label for="psw"><b>Password</b></label> -->
                            <input type="text" placeholder="نام خانوادگی" name="name" required>

                            <!-- <label for="psw-repeat"><b>Repeat Password</b></label> -->
                            <input type="text" placeholder="ایمیل" name="email" required>
                            <input type="text" placeholder="شماره همراه" name="phone" required>
                            <input type="password" placeholder="رمز" name="psw" required>
                            <div class="custom-select" style="margin-bottom:5%">
                                <select>
                                    <option value="0">تخصص</option>
                                    <option value="1">مشاور قراردادها</option>
                                    <option value="2">مشاور چک و اسناد تجاری</option>
                                    <option value="3">مشاور مالیاتی شرکتها</option>
                                    <option value="4">مشاور حقوقی شرکتها</option>
                                    <option value="5">مشاور داوری</option>
                                    <option value="6">مشاور دعاوی</option>
                                    <option value="7">مشاور حقوق خانواده</option>
                                    <option value="8">مشاور مالیاتی صنفی</option>
                                    <option value="9">مشاور طرح توجیهی</option>
                                </select>
                            </div>
                            <br>
                            <input type="text" placeholder="سایر تخصص ها" name="email" required>
                            <div class="custom-select">
                                <select>
                                    <option value="0">شغل</option>
                                    <option value="1">وکیل دادگستری</option>
                                    <option value="2">کارشناس رسمی دادگستری</option>
                                    <option value="3">سردفتر اسناد رسمی</option>
                                    <option value="4">مترجم رسمی دادگستری</option>
                                    <option value="5">مشاور رسمی مالیاتی</option>
                                    <option value="6">کارگذار گمرکی(حق العمل کار)</option>
                                    <option value="7">سایر مشاغل</option>
                                    <option value="8">مدرس دانشگاه</option>
                                    <option value="9">کارشناس حقوقی</option>
                                    <option value="10">کارشناس مالیاتی</option>
                                </select>
                            </div>
                            <hr>
                            <div id="adviserPanel" style="text-align: right;">
                                <input type="checkbox" name="rule" id="advisorRule" value="1">
                                <label class="control-label">
                                    قوانین
                                    را مطالعه کردم و پذیرفتم.
                                </label>
                                <a data-toggle="modal" href="#myModal3" class="registerbtn" style="margin-right: 16.7%;">مشاهده قوانین</a>
                            </div>
                            <div id="adviserPanel" style="text-align: right;margin-top:3.5%">
                                <input type="checkbox" name="rule" id="advisorRule" value="1">
                                <label class="control-label">
                                    قراردادها
                                    را مطالعه کردم و پذیرفتم.
                                </label>
                                <a data-toggle="modal" href="#myModal3" class="registerbtn" style="margin-right: 10%;">مشاهده قراردادها</a>
                            </div>
                            <button type="submit" class="registerbtn">ثبت نام</button>
                        </div>
                        
                        <div class="container signin">
                            <p>اگر قبلا ثبت نام کرده اید وارد شوید : <a href="#">ورود</a></p>
                        </div>
                    </form>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn " data-dismiss="modal">انصراف</button>
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal Login -->
    <div class="modal" id="myModal2">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">فرم ثبت نام</h4>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <form action="/action_page.php">
                        <div class="container" style="max-width:95%;width:95%">
                            <input type="text" placeholder="شماره همراه" name="phone" required>
                            <input type="password" placeholder="رمز" name="psw" required>                            
                            <hr>
                            <button type="submit" class="registerbtn">ورود</button>
                        </div>
                        
                        <div class="container signin">
                            <p>اگر قبلا ثبت نام نکرده اید ثبت نام کنید : <a href="#">ثبت نام</a></p>
                        </div>
                    </form>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn " data-dismiss="modal">انصراف</button>
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal Rules -->
    <div class="modal" id="myModal3">
        <div class="modal-dialog" style="max-width: 1000px;text-align: right;">
            <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">فرم ثبت نام</h4>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <p>
                        1- مشاوره آنلاین، صرفا از ساعت 7 تا 24 ارائه می شود.
                    </p>
                    <p>
                        2- رزرو نوبت مشاوره از ساعت 7  تا 24 قابل انجام است.
                    </p>
                    <p>
                        3- زمان ارائه مشاوره حضوری در آنی مشاور از ساعت 9 تا 20 خواهد بود.
                    </p>
                    <p>
                        4- زمان ارائه مشاوره تلفنی و نوشتاری (چت) در آنی مشاور از ساعت 7 تا 24 خواهد بود.
                    </p>
                    <p>
                        5- آخرین ساعت رزرو مشاوره نوشتاری در آنی مشاور ساعت 23:30 و حداکثر زمان قابل انتخاب برای مشاوره نوشتاری در این بازه زمانی، فقط نیم ساعت خواهد بود.
                    </p>
                    <p>
                        6- به طور کلی در آنی مشاور، رزرو وقت مشاوره نوشتاری یا تلفنی در هر زمانی که انجام شود مشاوره نوشتاری یا تلفنی به طور خودکار در راس ساعت 24 قطع خواهد شد.
                    </p>
                    <p>
                        7- مشاوره حضوری در روزهای غیرتعطیل شنبه تا 4 شنبه از ساعت 9 الی 20 در نیمه نخست سال و از ساعت 9 تا 18 در نیمه دوم سال در  تهران، میدان هفت تیر، ابتدای خیابان بهارشیراز، نرسیده به خیابان سلیمان خاطر، جنب اداره پست، پلاک ۴، طبقه ۴ واحد ۱۱ یا هر محل دیگری که آنی مشاور تعیین می نماید قابل ارائه می باشد.
                    </p>
                    <p>
                        8- وجه مشاوره (حق المشاوره) پیش از آغاز مشاوره آنلاین یا در هنگام رزرو نوبت مشاوره، دریافت می شود.
                    </p>
                    <p>
                        9- اگر کاربر به هر دلیلی در موعد رزرو شده، در اتاق مشاوره حضور پیدا نکند با عنایت به اینکه عدم انجام مشاوره ناشی از عدم حضور کاربر بوده است هیچ مبلغی به کاربر، مسترد نخواهد شد.
                    </p>
                    <p>
                        10- لغو نوبت مشاوره تا دو روز پیش از نوبت مشاوره، بدون کسر مبلغ، امکان پذیر خواهد بود.
                    </p>
                    <p>
                        11- لغو نوبت مشاوره در یک روز پیش از نوبت مشاوره با کسر 25 درصد از وجه پرداختی، امکان پذیر خواهد بود.
                    </p>
                    <p>
                        12- نظر به اینکه لغو نوبت مشاوره از سوی کاربران، سبب ایجاد اختلال در برنامه کاری مشاوران و کارکرد صحیح آنی مشاور می گردد از این رو، اگر کاربر بیش از دو نوبت اقدام به لغو رزرو نوبت مشاوره نماید تا سه نوبت بعدی هزینه رزرو مشاوره او ده درصد بالاتر از هزینه معمول، محاسبه خواهد شد.
                    </p>
                    <p>
                        13- در صورت لغو مشاوره درخواستی کاربر از سوی مشاور یا آنی مشاور کل وجه پرداختی کاربر به حساب کاربری او یا به حساب بانکی اعلام شده از سوی او واریز و منظور خواهد شد.
                    </p>
                    <p>
                        14- در جهت بررسی کیفیت مشاوره ها و برنامه ریزی برای ارائه مشاوره های بهتر، همه مکالمه ها و اطلاعات مبادله شده از سوی آنی مشاور ضبط و حفظ می شود و به صورت کاملاً محرمانه نگهداری می گردد .
                    </p>
                    <p>
                        15- آنی مشاور به منظور حفظ کیفیت مشاوره ها باور دارد که مشاورانش پس از دو ساعت مشاوره متوالی باید نیم ساعت، فرصت استراحت فکری و جسمی داشته باشند و به همین دلیل، امکان رزرو مشاوره متوالی بیش از دو ساعت برای یک مشاور را فراهم نمی کند. پس از هر دو ساعت مشاوره متوالی، طی یک فاصله نیم ساعته به منظور استراحت مشاور، امکان رزرو نوبت مشاوره وجود نخواهد داشت.
                    </p>
                    <p>
                        16- آنی مشاور، به طور کلی، مسئولیتی در قبال عدم نگهداری ایمن نام کاربری و رمز عبور و تغییر شماره تلفن همراه کاربران ندارد. با این وجود، آنی مشاور بنا به خط مشی همیشگی خود برای بروزرسانی مستمر اطلاعات و داده های لازم برای حفظ ارتباط با کاربران، طی دوره های منظم یا در هنگام ارائه سرویس ها و خدمات خود یا در هر زمان که ضرورت داشته باشد از کاربران خواهد خواست که با ورود به پنل کاربری خویش، تغییر شماره تلفن همراه یا تغییر در حساب و رمز عبور خود را برابر رویه آنی مشاور اعمال نمایند.
                    </p>
                    <p>
                        17- فعالیت افرادی که برای آنی مشاور محرز گردد غیر رشید و محجور هستند در آنی مشاور ممنوع می باشد.
                    </p>
                    <p>
                        18- ثبت نام مشاور و کاربر به خودی خود به معنی آن است که مشاوران و کاربران برابر قانون یا احکام قضایی و قانونی از فعالیت در فضای مجازی و ارائه یا دریافت خدمات مشاوره در فضای مجازی و بستر الکترونیکی منع نشده اند. تخلف از این شرط، حق درخواست جبران خسارات وارده را برای آنی مشاور و افراد و...در پی خواهد داشت. در هر زمان که آنی مشاور تشخیص دهد که کاربران یا مشاوران دارای منع قانونی یا قضایی در این زمینه هستند ضمن مسدود نمودن حساب کاربری آنان، ارائه خدمات آنی مشاور به آنان و فعالیت آنان در آنی مشاور را بلافاصله، متوقف و خسارات و ناشی از تخلف آنان از شروط اعلامی آنی مشاور از جمله شروط و تعهدات هنگام ثبت نام و این تعهدات را مطالبه خواهد نمود.
                    </p>
                    <p>
                        19- ثبت نام مشاور و کاربر در آنی مشاور به خودی خود به معنی آن است که مشاوران و کاربران در انجام مشاوره به هر شکل که باشد ملتزم به رعایت ادب و احترام نسبت به یکدیگر و نسبت به همه افراد و اشخاص می باشند و از مبادله پیام و مستندات و مدارکی که مرتبط با موضوع مشاوره نیست یا برابر قوانین جمهوری اسلامی ایران وصف و عنوان مجرمانه دارد در بستر آنی مشاور پرهیز می نمایند.
                    </p>
                    <p>
                        20- ثبت نام مشاوران در آنی مشاور به خودی خود به منزله تعهد به ارائه مشاوره آنلاین، حداقل به میزان یک سوم کل زمان های ارائه مشاوره است.
                    </p>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <a href="#" id="checkRules" data-dismiss="modal" class="btn">قوانین را مطالعه کردم و پذیرفتم.</a>
                </div>
            </div>
        </div>
    </div>
</div>