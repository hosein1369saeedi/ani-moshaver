<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/header.php");?>
        <div class="header-image">
            <img src="images/sample.png" alt="">
        </div>
        <div class="container">
            <div class="ddd">
                <hr class="hs-h">
                <div class="bradkam">
                    <span><img src="images/logo/logo.png" alt=""> جزییات مشاور</span>            
                </div>
            </div>
        </div>
        <div class="detailsperson">
            <div class="container">
                <div class="side-left">
                    <div class="topdetail">
                        <div class="top">
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="bottom">
                            <div class="row">
                                <div class="col-md-2 text-center">
                                    <img class="img" src="images/user.png" alt="">
                                    <button type="button" class="btn btn-success">
                                        آنلاین هستم
                                    </button>
                                </div>
                                <div class="col-md-7 text-right pt-2">
                                    <h5>سید حسین سعیدی</h5>
                                    <p>مشاور چک و اسناد تجاری</p>
                                </div>
                                <div class="col-md-3 text-right pt-4">
                                    <ul>
                                        <li>
                                            <a class="btn btn-ani" style="padding: 0;width: 70%;">شروع گفت گو <img src="images/icon/back.png" alt="" style="width:10%"></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="bottomdetail topdetail pr-4 py-2">
                        <h5 class="bold">درباره مشاور</h5>
                        <p class="my-1">تستی تستی تستی</p>
                        <p class="my-1">تستی تستی تستی تستی تستی تستی تستی تستی تستی</p>
                        <p class="my-1">تستی تستی تستیتستی تستی تستی</p>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
    </body>
    <?php include("blocks/script.php");?>
</html>