$(function() {
	var header = $(".menu-area");
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();
		if (scroll >= 50) {
			header.addClass("scrolled");
		} else {
			header.removeClass("scrolled");
		}
	});
});

(function($){
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	    if (!$(this).next().hasClass('show')) {
		    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	    }
	    var $subMenu = $(this).next(".dropdown-menu");
	    $subMenu.toggleClass('show');

	    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
	       $('.dropdown-submenu .show').removeClass("show");
	    });

	    return false;
	});
})(jQuery)


function openCity(evt, cityName) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
	  tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
	  tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(cityName).style.display = "block";
	evt.currentTarget.className += " active";
}


// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}


// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

