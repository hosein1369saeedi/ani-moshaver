<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/header.php");?>
        <?php include("blocks/slider.php");?>
        <?php include("blocks/services.php");?>
        <?php include("blocks/subjects.php");?>
        <?php include("blocks/animoshaver.php");?>
        <?php include("blocks/advice.php");?>
        <?php include("blocks/animation.php");?>
        <?php include("blocks/footer.php");?>
    </body>
    <?php include("blocks/script.php");?>
</html>